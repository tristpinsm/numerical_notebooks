
# coding: utf-8

# # Fluids week 4 
# ## Problem 5

# In[1]:

import numpy as np
from matplotlib.pyplot import *

""" These functions solve problem 5.
    At the end of the file (in the if __name__ ... statement) lives some code that 
    you can run as a script. Run "python prob5.py" to see the results pop up.
    You can also edit the parameters in that block to see the effect.
"""


# ### Define finite difference functions

# T is a vector of temperature in each space cell at a given time (shape (Nx,)).  
# First and last elements are kept fixed at their given values, i.e. fixed boundary conditions.  
# The returned vector excludes the boundaries.

def advect(T, v, dx, dt):
    # Leave both boundaries fixed
    return 0.5 * ( (T[:-2] + T[2:]) - v * dt / dx * (T[2:] - T[:-2]) )
def diffuse(T, D, dx, dt):
    # Leave both boundaries fixed
    return T[1:-1] + D * dt / dx**2 * (T[2:] - 2*T[1:-1] + T[0:-2])


# ### Stability conditions
# $$ \frac{2 \Delta t D}{(\Delta x)^2} \lt 1 $$
# $$ \frac{v \Delta t}{\Delta x} \lt 1 $$

# ## Time loop into a single function
def show_solution(Nt, Nx, dt, dx, v, D, T_left, T_right, fname):
    # Initialize grid
    T = np.zeros( (Nt, Nx) )  # Each time interval is will be operated on twice
    T[0,:-1] = T_left  # at t=0
    T[:,0] = T_left
    T[:,-1] = T_right  # all time
    
    # Iterate over time steps
    for t in range(1, Nt):
        # advection step
        T[t][1:-1] = advect(T[t-1], v, dx, dt)
        # diffusion step
        T[t][1:-1] = diffuse(T[t], D, dx, dt)
    imshow(T, aspect='auto', cmap='winter', interpolation='none')
    ylabel("Time")
    xlabel("Distance")
    cb = colorbar()
    cb.set_label("Temperature")
    savefig(fname, dpi=300, figsize=(12,5))
    clf()
    return T

# The steady state solution (t->inf) can be solved analytically
def steady_state(Tl, Tr, D, v, x):
    return Tl + np.exp(-v/D*100) * (-1 + (Tr-Tl) * np.exp(v/D*x))

if __name__ == "__main__":
    # Parameters
    # ----------
    Nt=200
    Nx=100
    dt=1
    dx=1
    v=0.1
    D=0.5
    T_left=1
    T_right=2
    # Numerically solve with these parameter
    T = show_solution(Nt, Nx, dt, dx, v, D, T_left, T_right, fname="prob5_result.png")

    # Now, look at what happens if we play with the time step
    # Start with this:
    T_1 = show_solution(Nt=200, Nx=100, dt=1, dx=1, v=0.1, D=0.5, T_left=1, T_right=2, fname="prob_5_ex1.png")
    # Use finer time step, but larger number of steps, so that physical system is the same
    T_2 = show_solution(Nt=2000, Nx=100, dt=0.1, dx=1, v=0.1, D=0.5, T_left=1, T_right=2, fname="prob_5_ex2.png")
    # If you look at the images, they are different!
    # This can be explained by "numerical diffusion", a numerical effect that effectively increases the diffusion

    # Compare the steady state solutions
    # We can compute the analytical solution:
    X = np.linspace(0,100,100)
    T_ss = steady_state(1,2,0.5,0.1,X)
    # Plot these
    fig = figure(figsize=(12,5))
    subplot(121)
    plot(T_1[-1,:], label='ex1')
    plot(X, T_ss, label='analytical')
    title("Numerical diffusion is not as significant")
    ylabel('Temperature')
    xlabel("Distance")
    legend()
    subplot(122)
    plot(T_2[-1,:], label='ex2')
    plot(X, T_ss, label='analytical')
    title('Numerical diffusion dominates')
    xlabel("Distance")
    legend()
    fig.tight_layout()
    savefig("prob_5_num_diff_steady_state.png", dpi=300)


